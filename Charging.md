
Clients will be charged each time they request data from the API.

Requesting a single data point costs one token, while requesting a list
of data points costs 3 tokens.

Graphing the data costs 5 tokens.

Higher token costs reflects the increased resource cost for certain
requests.

Each user starts with 20 tokens and more tokens can be purchased for
10 cents each.