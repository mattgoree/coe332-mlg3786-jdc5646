import uuid
import os
from flask import Flask, jsonify, request
from hotqueue import HotQueue
import json
import redis
import datetime
import matplotlib
import csv
matplotlib.use('Agg')
import matplotlib.pyplot as plt


SUBMITTED_STATUS = 'submitted'
IN_PROGRESS = 'in_progress'
COMPLETE_STATUS = 'complete'

REDIS_IP = os.environ.get('REDIS_IP', '172.17.0.1')
try:
    REDIS_PORT = int(os.environ.get('REDIS_PORT'))
except:
    REDIS_PORT = 6379
    
rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=0, decode_responses=True)
graph_db = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db = 2)
queue = HotQueue("jobs_queue", host = REDIS_IP, port=REDIS_PORT, db=1)

#172.17.0.1

def between(start = 0, end = 3020):
    """Returns a list of points between the specified years"""
      
    rainfall = readFile()
    ans = []
    for p in rainfall:
        if p['year'] >= start and p['year'] <= end:
            ans.append(p)
    return ans

def inRange(limit = None, offset = 0):
    """Returns a list of points from the given offset up to the 
    given limit"""
    
    if limit == "":
        limit = None
    if offset == "":
        offset = 0
    
    rainfall = readFile()
    if limit is None:
        limit = len(rainfall)
    ans = []
    for p in rainfall:
        if p['id'] in range(offset,offset+limit):
            ans.append(p)
    return ans

def readFile():
    """Reads the entire csv file and returns a list of data
    points"""
    
    with open("rainfall.csv",'r') as f:
        lines = f.readlines()
        allData = []
        index = 0
        for line in lines:
            data = {}
            x = line.split(",")
            data['id'] = index
            data['year'] = int(x[0])
            data['rain'] = float(x[1])
            allData.append(data)
            index = index + 1
    return allData

def add_datapoint(year,value):
    """Appends a given datapoint to the csv file and saves it"""
    
    try:
        with open("rainfall.csv", 'a') as f:        
            f.write('\n{0},{1}'.format(year,value))        
        return {"message" : "Data point ({0}, {1}) added successfully".format(year,value)}
    except:
        return {"message" : "Data point unable to be added."}
        
def queue_job(jid):
    queue.put(jid)
    
def generate_jid():
    return str(uuid.uuid4())

def instantiate_job(jid, status, start, end, limit, offset, plot):
    return {'id': jid,
            'status': status,
             'start': start,
             'end': end, 
             'limit': limit,
             'offset': offset,
             'create time': str(datetime.datetime.now()),
             'last update time': str(datetime.datetime.now()),
             'plot': plot
             }

def generate_job_key(jid):
    return 'job.{}'.format(jid)

def add_job(jid, status, start, end, limit, offset, plot=""):
    job_dict = instantiate_job(jid, status, start, end, limit, offset, plot)
    rd.hmset(generate_job_key(jid), job_dict)
    queue_job(jid)
    
    return job_dict

def get_job_by_id(jid):
    return rd.hgetall(generate_job_key(jid))

def get_all_jobs():
    all_jobs = []
    for key in rd.keys():
        #job = get_job_by_id(key)
        job = rd.hgetall(key)
        all_jobs.append(job)
    return all_jobs

def get_job_plot(jid, type):
    job = get_job_by_id(jid)

    if not job['status'] == COMPLETE_STATUS:
        return "Job is not completed"
    else:
        if type == "normal":
            return graph_db.hmget(generate_job_key(jid) + '_normal', 'plot_normal')
        elif type == "color":
            return graph_db.hmget(generate_job_key(jid) + '_color', 'plot_color')
    

def finalize_job(jid, file_path_normal, file_path_color):
    """Reads the temporary plot file as binary data and posts it
    to the graph db and updates the job status to completed"""
    
    plot_normal = {"plot_normal" : open(file_path_normal, 'rb').read()}
    plot_color = {"plot_color" : open(file_path_color, 'rb').read()}
    job = get_job_by_id(jid)
    job['status'] = COMPLETE_STATUS
    job['plot'] = "Generated"

    graph_db.hmset(generate_job_key(jid)+'_normal', plot_normal)
    graph_db.hmset(generate_job_key(jid)+'_color', plot_color)
    rd.hmset(generate_job_key(jid),job)

def update_job_status(jid, status):
    job = get_job_by_id(jid)
    if job:
        job['status'] = status
        rd.hmset(generate_job_key(jid),job)
    else:
        raise Exception()

def fix_job_input(job):
    try:
        job['start'] = int(job['start'])
    except:
        job['start'] = 0
    try:
        job['end'] = int(job['end'])
    except:
        job['end'] = 2020
    try:
        job['limit'] = int(job['limit'])
    except:
        job['limit'] = ""
    try:
        job['offset'] = int(job['offset'])
    except:
        job['offset'] = 0
        
    return job

def get_average(points_list):
    """Returns the average rainfall for a given subset of datapoints"""
    
    num_points = len(points_list)
    total_rain = sum([p['rain'] for p in points_list])
    return total_rain / num_points

def execute_job(jid):
    job = get_job_by_id(jid)
    job = fix_job_input(job)
    
    if job['start'] or job['end']:
        points = between(start = job['start'], end = job['end'])
    if job['limit'] or job['offset']:
        points = inRange(limit = job['limit'], offset = job['offset'])
        
    #Generate normal plot
    years = [p['year'] for p in points]
    rainfall = [p['rain'] for p in points]
    plt.scatter(years, rainfall)
    plt.title("Annual Rainfall in Fortaleza Brazil", fontsize=18)        
    plt.xlabel("Year")
    plt.ylabel("Rainfall (Inches)")
    tmp_file_normal = '/tmp/{0}.png'.format((jid + "_normal"))
    plt.savefig(tmp_file_normal, dpi = 150)
    plt.close()

    #Generate colored plot
    average = get_average(points)    
    blue_years = [p['year'] for p in points if p['rain'] >= average]
    blue_points = [p['rain'] for p in points if p['rain'] >= average]
    red_years = [p['year'] for p in points if p['rain'] < average]
    red_points = [p['rain'] for p in points if p['rain'] < average]
    
    plt.scatter(blue_years, blue_points, color='blue')
    plt.scatter(red_years, red_points, color='red')
    plt.title("Annual Rainfall in Fortaleza Brazil", fontsize=18)
    plt.xlabel("Year")
    plt.ylabel("Rainfall (Inches)")
    plt.legend(["Above Average", "Below Average"])
    tmp_file_color = '/tmp/{0}.png'.format((jid + "_color"))
    plt.savefig(tmp_file_color, dpi = 150)
    
    finalize_job(jid, tmp_file_normal, tmp_file_color)


