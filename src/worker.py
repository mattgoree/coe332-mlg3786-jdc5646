import uuid
from flask import Flask, jsonify, request
from hotqueue import HotQueue
import json
import redis
import datetime
import jobs
from jobs import queue, update_job_status, IN_PROGRESS, COMPLETE_STATUS, execute_job as execute


@queue.worker
def execute_job(jid):

    update_job_status(jid, IN_PROGRESS)
    execute(jid)

    update_job_status(jid, COMPLETE_STATUS)
    
    
    
        
execute_job()
#172.17.0.1
