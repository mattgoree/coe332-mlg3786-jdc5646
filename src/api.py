from flask import Flask, jsonify, request, send_file
import redis
import jobs
import json
import io
from jobs import between, inRange, readFile, SUBMITTED_STATUS, REDIS_IP, REDIS_PORT, add_datapoint

app = Flask(__name__)
rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=0, decode_responses=True)
graph_db = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db = 2)


@app.route('/rain', methods = ['GET'])
def rain():
    """Returns a list of every datapoint in the csv file"""    
    error_dict = {}
    error_dict['status'] = "Error"
    error_dict['message'] = ""
    start = request.args.get('start')
    end = request.args.get('end')
    offset = request.args.get('offset')
    limit = request.args.get('limit')
    if start:
        try:
            start = int(start)
        except:
            error_dict['message'] = "start and end, if provided, must be integers"
            return jsonify(error_dict), 400
    if end:
        try:
            end = int(end)
        except:
            error_dict['message'] = "start and end, if provided, must be integers"
            return jsonify(error_dict), 400
    if offset:
        try:
            offset = int(offset)
        except:
            error_dict['message'] = "limit and offset, if provided, must be integers"
            return jsonify(error_dict), 400
    if limit:
        try:
            limit = int(limit)
        except:
            error_dict['message'] = "limit and offset, if provided, must be integers"
            return jsonify(error_dict), 400
    if (start or end) and (limit or offset):
        error_dict['message'] = "limit and/or offset cannot be combined with start and/or end"
        return jsonify(error_dict), 400
    if start and end:
        return jsonify(between(start = start, end = end))
    if start:
        return jsonify(between(start = start))
    if end:
        return jsonify(between(end = end))
    if offset and limit:
        return jsonify(inRange(offset = offset, limit = limit))
    if offset:
        return jsonify(inRange(offset = offset))
    if limit:
        return jsonify(inRange(limit = limit))
    return jsonify(readFile())

@app.route('/rain/<index>', methods = ['GET'])
def id(index):
    """Returns a single datapoint with the given index"""
    return jsonify(inRange(limit = 1, offset = int(index)))

@app.route('/rain/year/<year>', methods = ['GET'])
def year(year):
    """Returns a single datapoint with the given year"""
    return jsonify(between(start = int(year), end = int(year)))

@app.route('/rain/insert', methods = ['POST'])
def add_data():
    """Appends a new datapoint to the end of the CSV file"""
    try:
        data = request.get_json(force=True)
    except Exception as e:
        return json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)}), 400
    year = data.get('year')
    rain = data.get('rain')
    try:
        year = int(year)
    except:
        return jsonify({'status': "Error", 'message': "year must be an integer."})
    try:
        rain = int(rain)
    except:
        return jsonify({'status': "Error", 'message': "rain must be an integer"})
    return jsonify(add_datapoint(year, rain))

@app.route('/jobs', methods=['GET', 'POST'])
def jobs_method():
    """GET returns list of all submitted jobs, POST submits a new job"""
    if request.method == 'POST':
        try:
            job = request.get_json(force=True)
        except Exception as e:
            return json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)}), 400
        start = job.get('start')
        end = job.get('end')
        limit = job.get('limit')
        offset = job.get('offset')
        if start:
            try:
                start = int(start)
            except:
                return jsonify({'status': 'Error', 'message': 'Start must be an integer.'}), 400
        if end:
            try:
                end = int(end)
            except:
                return jsonify({'status': 'Error', 'message': 'End must be an integer.'}), 400
        if limit:
            try:
                limit = int(limit)
            except:
                return jsonify({'status': 'Error', 'message': 'Limit must be an integer.'}), 400
        if offset:
            try:
                offset = int(offset)
            except:
                return jsonify({'status': 'Error', 'message': 'Offset must be an integer.'}), 400
        if (start or end) and (limit or offset):
            return jsonify({'status': 'Error', 'message': 'Start and/or end cannot be combined with limit and/or offset'}), 400
        if start == None:
            start = ""
        if end == None:
            end = ""
        if limit == None:
            limit = ""
        if offset == None:
            offset = ""
        result = jobs.add_job(jobs.generate_jid(), SUBMITTED_STATUS, start, end, limit, offset)
        return jsonify({'message': 'Job created successfully.', 'result': result})
    if request.method == 'GET':
        return jsonify(jobs.get_all_jobs())


@app.route('/jobs/<job_id>', methods=['GET'])
def get_jobs(job_id):
    """Returns the job with the given id""" 
    jid = jobs.generate_job_key(job_id)
    if jid in rd.keys():
        return jsonify(rd.hgetall(jid))
    else:
        return jsonify({'status': 'Error', 'message': 'Job id does not exist'}), 400

@app.route('/jobs/<job_id>/plot', methods=['GET'])
def job_plot(job_id):
    """Returns a file object of the plot of the specified job"""
    plot = jobs.get_job_plot(job_id, "normal")
    return send_file(io.BytesIO(plot[0]),
                     mimetype='image/png',
                     as_attachment=True,
                     attachment_filename='{}.png'.format(job_id))

@app.route('/jobs/<job_id>/color_plot', methods=['GET'])
def job_plot_color(job_id):
    """Returns a file object of the plot of the specified job with blue points above the average and red points below"""

    plot = jobs.get_job_plot(job_id, "color")
    return send_file(io.BytesIO(plot[0]),
                     mimetype='image/png',
                     as_attachment=True,
                     attachment_filename='{}.png'.format(job_id))

    
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
