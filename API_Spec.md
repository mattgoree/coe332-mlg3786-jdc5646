# coe332-mlg3786-jdc5646

## Endpoints

#### List all Data Points

    GET /rain
Get a list of all rainfall data

#### List Subset of Data Points

    GET /rain?start=<start>&end=<end>
Get a list of rainfall data between the specified start and end years

    GET /rain?limit=<limit>&offset=<offset>
Get a list of rainfall data after offset up to limit

    GET /rain/<id>
Get rainfall data for the specified id

    GET /rain/year/<year>
Get rainfall data for the specified year
    
#### Create a New Job

    POST /jobs
Create a new job with a dictionary of start and end values or limit and offset values and initiate creation of a data plot

#### List Jobs and Job Data

    GET /jobs
Get a list of all jobs

    GET /jobs/<id>
Get a job dictionary for the specified id

    GET /jobs/<id>/plot
Get a plot of the specified job if it has been created

    GET /jobs/<id>/color_plot
Get a plot of the specified job with blue points above the average rainfall for that subset and red points below the average
    
#### Upload New Data Points

    POST /rain/insert
Add a new data point with a given year and inches of rain

## Examples

GET /rain
```json
[
  {
    "id": 0,
    "rain": 101.0,
    "year": 1770
  },
        ...

  {
    "id": 99,
    "rain": 74.0,
    "year": 1864
  }
]
		
```
GET /rain?start=1784&end=1787
```json
[
 {
   "id": 14,
   "rain": 10,
   "year": 1784
 },
 {
   "id": 15,
   "rain": 24,
   "year": 1785
 },
 {
   "id": 16,
   "rain": 83,
   "year": 1786
 },
 {
   "id": 17,
   "rain": 132,
   "year": 1787
 }
]
```
GET /rain?limit=2&offset=37
```json
[
 {
   "id": 37,
   "rain": 10,
   "year": 1807
 },
 {
   "id": 38,
   "rain": 8,
   "year": 1808
 }
]
```
GET /rain/13
```json
{
  "id": 13,
  "rain": 13.0,
  "year": 1790
}
```
GET /sunspots/rain/1794
```json
{
  "id": 23,
  "rain": 65.0,
  "year": 1794
}
```

## Accessing API

#### Curl

**curl localhost:5000/sunspots/year/1794**
```json
[ 
     {
       "id": 23,
       "sunspots": 65.0,
       "year": 1794
     }
    ]
```
**curl localhost:5000/sunspots?limit=2&offset=37**
```json
    [
     {
       "id": 37,
       "sunspots": 10,
       "year": 1807
     },
     {
       "id": 38,
       "sunspots": 8,
       "year": 1808
     }
    ]
```
**curl -d '{"start": 1980}' localhost:5000/jobs**
```json
    {
     "message": "Job created successfully.",
     "result": {
       "create time": "2018-12-13 20:14:21.879097",
       "end": "",
       "id": "85174438-f85f-4d0f-bba5-4c240802f040",
       "last update time": "2018-12-13 20:14:21.879120",
       "limit": "",
       "offset": "",
       "plot": "",
       "start": 1980,
       "status": "submitted"
     }
    }
```
**curl localhost:5000/jobs/85174438-f85f-4d0f-bba5-4c240802f040**
```json
    {
     "create time": "2018-12-13 20:14:21.879097",
     "end": "",
     "id": "85174438-f85f-4d0f-bba5-4c240802f040",
     "last update time": "2018-12-13 20:14:21.879120",
     "limit": "",
     "offset": "",
     "plot": "Generated",
     "start": "1980",
     "status": "complete"
    }
```
**curl localhost:5000/jobs/85174438-f85f-4d0f-bba5-4c240802f040/plot --output plot.png**
Save the plot as a png file in the current directory

**curl localhost:5000/jobs/85174438-f85f-4d0f-bba5-4c240802f040/color_plot --output color_plot.png**
Save the color plot as a png file in the current directory


#### Python
```python
import requests
response = requests.get('http://localhost:5000/rain/year/1800')
print(response.content)
```
```json
b'{\n "id": 30, \n "rain": 15.0, \n "year": 1800\n}\n'
```
```python
import requests
response = requests.get('http://localhost:5000/rain?offset=2&limit=1')
print(response.content)
```
```json
b'[\n   "id": 2, \n   "rain": 67.0, \n   "year": 1772\n}]
```

