This project provides an api for interpreting data from a time series of rainfall data.
Authors: Matthew Goree and Josh Collins

Documentation for how to use this API is given in API_Spec.md

## Extra Credit Analysis

For extra credit we added the following functionality:

Each job's datapoints are used to compute an average rainfall
value. Each datapoint is then compared against this average.
All points that have values greater or equal to the average
are plotted with blue dots, while all points that have smaller
values are plotted with red dots.

A plot of this type is retrieved through the following endpoint:

  '/jobs/<job_id>/color_plot'

## Deployment Instructions

### Single VM Deployment

In the top level of the repository, run the following command:

   'docker-compose up'

This launches the Redis container and project_image container.
An easy way to test if it is running is to run the following
command in another terminal:

	'curl localhost:5000/rain'

This should return a list of all datapoints.


### Double VM Deployment

Clone this repository to both VM's you intend to run the
service on. In the VM which you intend to use as the host
of the worker service, make the following changes to the file:
docker-compose-worker.yml

On line 11 of 'docker-compose-worker.yml', you MUST change
'REDIS_IP=10.0.2.31' to the private redis ip of the VM you
are using to host the Flask API service.

Then run the following command on the VM you are using to host
the Flask API service:

    'docker-compose -f docker-compose-api.yml up'

Next, run the following command on the second VM which will host
the worker service:

    'docker-compose -f docker-compose-worker.yml up'

